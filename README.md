
# Atelier Flexbox

Pour ce challenge vous allez reproduire la page d'acceuil du site: https://slack.com en utilisant Flexbox.  

Adapter la page aux écrans de différentes tailles:  
* (width < 768px)
![alt text](/screenshots/1.png) 
* (961px > width > 767px)
![alt text](/screenshots/2.png) 
* (1024px > width > 960px)
![alt text](/screenshots/3.png) 
* (width > 1024px)  
![alt text](/screenshots/4.png) 



